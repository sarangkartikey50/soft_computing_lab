from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets
from sklearn.metrics import accuracy_score

iris_data = (datasets.load_iris())

target_names = iris_data.target_names

X_train, X_test, y_train, y_test = train_test_split(iris_data.data , iris_data.target, test_size=0.33, random_state=42)

knn = KNeighborsClassifier()
knn.fit(X_train, y_train)

predicted = knn.predict(X_test)

print(accuracy_score(y_test, predicted))


